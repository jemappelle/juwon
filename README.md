Hallo,
diese Zeilen sind nicht ohne Grund auf deutsch geschrieben.
Nur wer sie versteht hat Grund mich zu kontaktieren. Aber die
Sprache muss man nicht verstehen. Eigentlich war ich nur zu
faul den letzten Text auf Englisch zu verfassen.

Du hast es weit gebracht und bist nun am Ende angekommen, also
hast du dir nun etwas verdient. Vorrausgesetzt du bist einer der
ersten fünf die es geschafft haben.

Ich habe ein Geschenk für dich, aber dieses kann ich dir nicht
überreichen, solange du dich nicht bei mir gemeldet hast. Nur
wie willst du dies tun? Ganz einfach: Du kannst mir eine E-Mail
schreiben, mich anrufen oder mich sogar besuchen. Aber wie nur,
wenn du nicht mal meinen Namen kennst? Auch ganz einfach. Ich
habe dir meine Email (Und nein, es ist nicht die Email die für
Commits in diesem Git genutzt wurde, das wäre ja schon sehr dumm...)
längst verraten, du musst mir nur noch schreiben, aber beeil dich.